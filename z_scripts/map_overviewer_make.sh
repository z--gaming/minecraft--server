#!/bin/sh

cd /home/mikko/minecraft-server/

CMD="overviewer.py --config=./overviewer_config.py"

local_mov=$(which overviewer.py > /dev/null 2> /dev/null && echo yes || echo no)

if [ $local_mov = 'yes' ]; then
  bash -c "$CMD"
else
  podman run -it \
    -v ./overviewer_config.py:/home/mikko/app/overviewer_config.py \
    -v ./Utopia:/home/mikko/app/Utopia \
    -v ./public/map:/home/mikko/app/public/map \
    -v ~/.minecraft:/home/mikko/.minecraft \
    -t localhost/overviewer \
    /bin/bash -c "$CMD"
fi
