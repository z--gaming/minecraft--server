#!/bin/sh

cd /home/mikko/minecraft-server/

podman build \
  --build-arg USER=$USER \
  --build-arg UID=$(id -u) \
  --build-arg GID=$(id -g) \
  --build-arg PW=1234 \
  -t localhost/overviewer \
  -f ./z_scripts/overviewer_setup/Dockerfile
