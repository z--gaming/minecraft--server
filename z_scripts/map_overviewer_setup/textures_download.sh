#!/bin/sh

VERSION=1.16.5

mkdir -p ~/.minecraft/versions/${VERSION} 2> /dev/null

wget https://overviewer.org/textures/${VERSION} \
  -O /home/mikko/.minecraft/versions/${VERSION}/${VERSION}.jar
