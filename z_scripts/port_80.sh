#!/bin/sh

# sudo /usr/sbin/iptables-restore /etc/sysconfig/iptables

sudo /usr/sbin/iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 8080
sudo /usr/sbin/iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080

# sudo sh -c '/usr/sbin/iptables-save > /etc/sysconfig/iptables'
