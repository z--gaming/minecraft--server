#!/bin/sh

cd /home/mikko/minecraft-server/
rm ./public/logs/*.log.gz 2> /dev/null

# TODO: replace xml with yml (it looks much prettier)
# (https://logging.apache.org/log4j/2.x/runtime-dependencies.html)

exec java -server \
  -Xms1280M \
  -Xmx1280M \
  -Djava.net.preferIPv4Stack=true \
  -Dlog4j.configurationFile=./log4j2.xml \
  -XX:UseSSE=3 \
  -XX:-DisableExplicitGC \
  -XX:+UseParallelOldGC \
  -jar './fabric-server-launch.jar' nogui
